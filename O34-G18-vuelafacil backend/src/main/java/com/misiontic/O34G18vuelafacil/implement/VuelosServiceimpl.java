/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.implement;

import com.misiontic.O34G18vuelafacil.Models.Vuelos;
import com.misiontic.O34G18vuelafacil.Dao.VuelosDao;
import com.misiontic.O34G18vuelafacil.Service.VuelosService;



import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author FRANK
 */

@Service
public class VuelosServiceimpl implements VuelosService{
    @Autowired
    private VuelosDao vuelosDao;
    
    @Override
    @Transactional(readOnly=false)
    public Vuelos save(Vuelos vuelos){
        return vuelosDao.save(vuelos);
    }
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        vuelosDao.deleteById(id);
    }     

   @Override
   @Transactional(readOnly=true)
       public List <Vuelos>findAll(){
           return (List <Vuelos>)vuelosDao.findAll();  
    }   
    @Override
    @Transactional(readOnly=true)
    public Vuelos findById(Integer id){
        return vuelosDao.findById(id).orElse(null);
    }  
    
}
