/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.implement;

import com.misiontic.O34G18vuelafacil.Models.Pasajes;
import com.misiontic.O34G18vuelafacil.Dao.PasajesDao;
import com.misiontic.O34G18vuelafacil.Service.PasajesService;



import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author FRANK
 */

@Service
public class PasajesServiceimpl implements PasajesService{
    
      
    @Autowired
    private PasajesDao pasajesDao;
    
    @Override
    @Transactional(readOnly=false)
    public Pasajes save(Pasajes pasajes){
        return pasajesDao.save(pasajes);
    }
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        pasajesDao.deleteById(id);
    }     

   @Override
   @Transactional(readOnly=true)
       public List <Pasajes>findAll(){
           return (List <Pasajes>)pasajesDao.findAll();  
    }   
    @Override
    @Transactional(readOnly=true)
    public Pasajes findById(Integer id){
        return pasajesDao.findById(id).orElse(null);
    }  
}
