/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.implement;

import com.misiontic.O34G18vuelafacil.Models.Destino;
import com.misiontic.O34G18vuelafacil.Dao.DestinoDao;
import com.misiontic.O34G18vuelafacil.Service.DestinoService;



import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author FRANK
 */

@Service
public class DestinoServiceimpl implements DestinoService{
    
   @Autowired
    private DestinoDao destinoDao;
    
    @Override
    @Transactional(readOnly=false)
    public Destino save(Destino destino){
        return destinoDao.save(destino);
    }
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        destinoDao.deleteById(id);
    }     

   @Override
   @Transactional(readOnly=true)
       public List <Destino>findAll(){
           return (List <Destino>)destinoDao.findAll();  
    }   
    @Override
    @Transactional(readOnly=true)
    public Destino findById(Integer id){
        return destinoDao.findById(id).orElse(null);
    }  
    
}
