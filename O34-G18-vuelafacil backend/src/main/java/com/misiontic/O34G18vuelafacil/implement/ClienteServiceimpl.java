/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.implement;

import com.misiontic.O34G18vuelafacil.Models.Cliente;
import com.misiontic.O34G18vuelafacil.Dao.ClienteDao;
import com.misiontic.O34G18vuelafacil.Service.ClienteService;



import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author FRANK
 */

@Service
public class ClienteServiceimpl implements ClienteService{
    
    @Autowired
    private ClienteDao clienteDao;
    
    @Override
    @Transactional(readOnly=false)
    public Cliente save(Cliente cliente){
        return clienteDao.save(cliente);
    }
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        clienteDao.deleteById(id);
    }     

   @Override
   @Transactional(readOnly=true)
       public List <Cliente>findAll(){
           return (List <Cliente>)clienteDao.findAll();  
    }   
    @Override
    @Transactional(readOnly=true)
    public Cliente findById(Integer id){
        return clienteDao.findById(id).orElse(null);
    }  
}
