/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.implement;


import com.misiontic.O34G18vuelafacil.Models.Origen;
import com.misiontic.O34G18vuelafacil.Dao.OrigenDao;
import com.misiontic.O34G18vuelafacil.Service.OrigenService;



import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author FRANK
 */

@Service
public class OrigenServiceimpl implements OrigenService{
    
    @Autowired
    private OrigenDao origenDao;
    
    @Override
    @Transactional(readOnly=false)
    public Origen save(Origen origen){
        return origenDao.save(origen);
    }
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        origenDao.deleteById(id);
    }     

   @Override
   @Transactional(readOnly=true)
       public List <Origen>findAll(){
           return (List <Origen>)origenDao.findAll();  
    }   
    @Override
    @Transactional(readOnly=true)
    public Origen findById(Integer id){
        return origenDao.findById(id).orElse(null);
    }  
}
