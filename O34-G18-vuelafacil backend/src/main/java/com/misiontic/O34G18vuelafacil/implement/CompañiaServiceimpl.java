/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.implement;


import com.misiontic.O34G18vuelafacil.Models.Compañia;
import com.misiontic.O34G18vuelafacil.Dao.CompañiaDao;
import com.misiontic.O34G18vuelafacil.Service.CompañiaService;



import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author FRANK
 */
@Service
public class CompañiaServiceimpl implements CompañiaService{
    
    @Autowired
    private CompañiaDao compañiaDao;
    
    @Override
    @Transactional(readOnly=false)
    public Compañia save(Compañia compañia ){
        return compañiaDao.save(compañia);
    }
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        compañiaDao.deleteById(id);
    }     

   @Override
   @Transactional(readOnly=true)
       public List <Compañia>findAll(){
           return (List <Compañia>)compañiaDao.findAll();  
    }   
    @Override
    @Transactional(readOnly=true)
    public Compañia findById(Integer id){
        return compañiaDao.findById(id).orElse(null);
    }   
    
  
       
}
