/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.implement;


import com.misiontic.O34G18vuelafacil.Models.Itinerario;
import com.misiontic.O34G18vuelafacil.Dao.ItinerarioDao;
import com.misiontic.O34G18vuelafacil.Service.ItinerarioService;



import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author FRANK
 */
@Service
public class ItinerarioServiceimpl implements ItinerarioService{
    
    @Autowired
    private ItinerarioDao itinerarioDao;
    
    @Override
    @Transactional(readOnly=false)
    public Itinerario save(Itinerario itinerario){
        return itinerarioDao.save(itinerario);
    }
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        itinerarioDao.deleteById(id);
    }     

   @Override
   @Transactional(readOnly=true)
       public List <Itinerario>findAll(){
           return (List <Itinerario>)itinerarioDao.findAll();  
    }   
    @Override
    @Transactional(readOnly=true)
    public Itinerario findById(Integer id){
        return itinerarioDao.findById(id).orElse(null);
    }      
    
}
