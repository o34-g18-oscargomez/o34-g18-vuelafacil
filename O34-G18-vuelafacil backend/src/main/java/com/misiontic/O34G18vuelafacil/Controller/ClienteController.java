/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.Controller;

import com.misiontic.O34G18vuelafacil.Models.Cliente;
import com.misiontic.O34G18vuelafacil.Models.Vuelos;
import com.misiontic.O34G18vuelafacil.Models.Compañia;
import com.misiontic.O34G18vuelafacil.Models.Itinerario;
import com.misiontic.O34G18vuelafacil.Models.Pasajes;
import com.misiontic.O34G18vuelafacil.Models.Origen;
import com.misiontic.O34G18vuelafacil.Models.Destino;

import com.misiontic.O34G18vuelafacil.Service.ClienteService;
import com.misiontic.O34G18vuelafacil.Service.VuelosService;
import com.misiontic.O34G18vuelafacil.Service.CompañiaService;
import com.misiontic.O34G18vuelafacil.Service.ItinerarioService;
import com.misiontic.O34G18vuelafacil.Service.PasajesService;
import com.misiontic.O34G18vuelafacil.Service.OrigenService;
import com.misiontic.O34G18vuelafacil.Service.DestinoService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author FRANK
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/clientes")
public class ClienteController {
        
    @Autowired
    private ClienteService clienteservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Cliente> agregar (@RequestBody Cliente cliente){
    Cliente obj= clienteservice.save(cliente);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    
    }
    
    @DeleteMapping(value="/(id)")
    
    public ResponseEntity<Cliente> eliminar(@PathVariable Integer id){
    Cliente obj = clienteservice.findById(id);
    
    if (obj!= null){
        clienteservice.delete(id);
    }else{
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    } return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
   

    
@PutMapping(value="/")
    public ResponseEntity<Cliente> editar(@RequestBody Cliente cliente){
        Cliente obj = clienteservice.findById(cliente.getIdcliente());
        if(obj!=null){
            obj.setNombrecliente(cliente.getNombrecliente());
            obj.setEdad(cliente.getEdad());
            obj.setTelefonocliente(cliente.getTelefonocliente());
            obj.setEmailcliente(cliente.getEmailcliente());
            clienteservice.save(obj);
        }
        else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Cliente> consultarTodo(){
        return clienteservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Cliente consultarPorId(@PathVariable Integer id){
        return clienteservice.findById(id);
    }    
    
}