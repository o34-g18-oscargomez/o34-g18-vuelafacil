/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.Controller;

import com.misiontic.O34G18vuelafacil.Models.Cliente;
import com.misiontic.O34G18vuelafacil.Models.Vuelos;
import com.misiontic.O34G18vuelafacil.Models.Compañia;
import com.misiontic.O34G18vuelafacil.Models.Itinerario;
import com.misiontic.O34G18vuelafacil.Models.Pasajes;
import com.misiontic.O34G18vuelafacil.Models.Origen;
import com.misiontic.O34G18vuelafacil.Models.Destino;

import com.misiontic.O34G18vuelafacil.Service.ClienteService;
import com.misiontic.O34G18vuelafacil.Service.VuelosService;
import com.misiontic.O34G18vuelafacil.Service.CompañiaService;
import com.misiontic.O34G18vuelafacil.Service.ItinerarioService;
import com.misiontic.O34G18vuelafacil.Service.PasajesService;
import com.misiontic.O34G18vuelafacil.Service.OrigenService;
import com.misiontic.O34G18vuelafacil.Service.DestinoService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author FRANK
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/itinerarios")
public class ItinerarioController {
        
    @Autowired
    private ItinerarioService itinerarioservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Itinerario> agregar (@RequestBody Itinerario itinerario){
    Itinerario obj= itinerarioservice.save(itinerario);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    
    }
    
    @DeleteMapping(value="/(id)")
    
    public ResponseEntity<Itinerario> eliminar(@PathVariable Integer id){
    Itinerario obj = itinerarioservice.findById(id);
    
    if (obj!= null){
        itinerarioservice.delete(id);
    }else{
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    } return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
   

    
@PutMapping(value="/")
    public ResponseEntity<Itinerario> editar(@RequestBody Itinerario itinerario){
        Itinerario obj = itinerarioservice.findById(itinerario.getIditinerario());
        if(obj!=null){
            obj.setOrigen(itinerario.getOrigen());
            obj.setDestino(itinerario.getDestino());
            itinerarioservice.save(obj);
        }
        else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Itinerario> consultarTodo(){
        return itinerarioservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Itinerario consultarPorId(@PathVariable Integer id){
        return itinerarioservice.findById(id);
    }    
    
}   