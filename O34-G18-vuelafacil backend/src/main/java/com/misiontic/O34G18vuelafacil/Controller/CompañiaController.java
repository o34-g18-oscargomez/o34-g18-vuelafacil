/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.Controller;

import com.misiontic.O34G18vuelafacil.Models.Cliente;
import com.misiontic.O34G18vuelafacil.Models.Vuelos;
import com.misiontic.O34G18vuelafacil.Models.Compañia;
import com.misiontic.O34G18vuelafacil.Models.Itinerario;
import com.misiontic.O34G18vuelafacil.Models.Pasajes;
import com.misiontic.O34G18vuelafacil.Models.Origen;
import com.misiontic.O34G18vuelafacil.Models.Destino;

import com.misiontic.O34G18vuelafacil.Service.ClienteService;
import com.misiontic.O34G18vuelafacil.Service.VuelosService;
import com.misiontic.O34G18vuelafacil.Service.CompañiaService;
import com.misiontic.O34G18vuelafacil.Service.ItinerarioService;
import com.misiontic.O34G18vuelafacil.Service.PasajesService;
import com.misiontic.O34G18vuelafacil.Service.OrigenService;
import com.misiontic.O34G18vuelafacil.Service.DestinoService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author FRANK
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/compañias")
public class CompañiaController {
        
    @Autowired
    private CompañiaService compañiaservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Compañia> agregar (@RequestBody Compañia compañia){
    Compañia obj= compañiaservice.save(compañia);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    
    }
    
    @DeleteMapping(value="/(id)")
    
    public ResponseEntity<Compañia> eliminar(@PathVariable Integer id){
    Compañia obj = compañiaservice.findById(id);
    
    if (obj!= null){
        compañiaservice.delete(id);
    }else{
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    } return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
   

    
@PutMapping(value="/")
    public ResponseEntity<Compañia> editar(@RequestBody Compañia compañia){
        Compañia obj = compañiaservice.findById(compañia.getIdCompañia());
        if(obj!=null){
            obj.setNombrecompañia(compañia.getNombrecompañia());
            obj.setDircompañia(compañia.getDircompañia());
            obj.setTelefonocompañia(compañia.getTelefonocompañia());
            compañiaservice.save(obj);
        }
        else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Compañia> consultarTodo(){
        return compañiaservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Compañia consultarPorId(@PathVariable Integer id){
        return compañiaservice.findById(id);
    }    
    
}

