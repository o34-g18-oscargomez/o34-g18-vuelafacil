package com.misiontic.O34G18vuelafacil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class O34G18VuelafacilApplication {

	public static void main(String[] args) {
		SpringApplication.run(O34G18VuelafacilApplication.class, args);
	}

}
