/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.Dao;

import com.misiontic.O34G18vuelafacil.Models.Vuelos;
import org.springframework.data.repository.CrudRepository;
/**
 *
 * @author FRANK
 */
public interface VuelosDao extends CrudRepository<Vuelos,Integer>{
    
}
