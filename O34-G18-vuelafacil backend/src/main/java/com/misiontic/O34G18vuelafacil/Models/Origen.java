/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.Models;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import org.hibernate.annotations.GenericGenerator;


/**
 *
 * @author FRANK
 */


@Table
@Entity(name="Origen")

public class Origen implements Serializable{
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name=("idOrigen"))
    private Integer idorigen;
    
    @Column(name=("aereopuertoOrigen"))
    private String aereopuertoorigen;
    
    @Column(name=("ciudadOrigen"))
    private String ciudadorigen;    

    public Integer getIdorigen() {
        return idorigen;
    }

    public void setIdorigen(Integer idorigen) {
        this.idorigen = idorigen;
    }

    public String getAereopuertoorigen() {
        return aereopuertoorigen;
    }

    public void setAereopuertoorigen(String aereopuertoorigen) {
        this.aereopuertoorigen = aereopuertoorigen;
    }

    public String getCiudadorigen() {
        return ciudadorigen;
    }

    public void setCiudadorigen(String ciudadorigen) {
        this.ciudadorigen = ciudadorigen;
    }
    
    
    
    
}
