/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.Models;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;



/**
 *
 * @author FRANK
 */
@Table
@Entity(name="Vuelos")

public class Vuelos implements Serializable{
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name=("idVuelos"))
    private Integer idvuelos; 
    
    @ManyToOne
    @JoinColumn(name="idCompañia")
    private Compañia compañia;
    
    @ManyToOne
    @JoinColumn(name="idItinerario")
    private Itinerario itinerario;    
    
    @Column(name=("cantidadAsientos"))
    private String cantidadasientos;

    public Integer getIdvuelos() {
        return idvuelos;
    }

    public void setIdvuelos(Integer idvuelos) {
        this.idvuelos = idvuelos;
    }

    public Compañia setCompañia() {
        return compañia;
    }

    public void setCompañia(Compañia compañia) {
        this.compañia = compañia;
    }

    public Itinerario getItinerario() {
        return itinerario;
    }

    public void setItinerario(Itinerario itinerario) {
        this.itinerario = itinerario;
    }

    public String getCantidadasientos() {
        return cantidadasientos;
    }

    public void setCantidadasientos(String cantidadasientos) {
        this.cantidadasientos = cantidadasientos;
    }
    
    
    
}
