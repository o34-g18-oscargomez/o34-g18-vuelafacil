/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.Models;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Table
@Entity(name="Pasajes")

/**
 *
 * @author FRANK
 */
public class Pasajes implements Serializable{
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name=("idPasajes"))
    private Integer idpasajes;  
    
    @ManyToOne
    @JoinColumn(name="idCliente")
    private Cliente cliente;
    
    @ManyToOne
    @JoinColumn(name="idVuelos")
    private Vuelos vuelos;    
    
    @Column(name=("claseTiquete"))
    private String clasetiquete;
   
    @Column(name=("numAsiento"))
    private String numasiento;
    
    @Column(name=("valorPasaje"))
    private Double valorpasaje;

    public Integer getIdpasajes() {
        return idpasajes;
    }

    public void setIdpasajes(Integer idpasajes) {
        this.idpasajes = idpasajes;
    }

   public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Vuelos getVuelos() {
        return vuelos;
    }

    public void setVuelos(Vuelos vuelos) {
        this.vuelos = vuelos;
    }

    public String getClasetiquete() {
        return clasetiquete;
    }

    public void setClasetiquete(String clasetiquete) {
        this.clasetiquete = clasetiquete;
    }

    public String getNumasiento() {
        return numasiento;
    }

    public void setNumasiento(String numasiento) {
        this.numasiento = numasiento;
    }

    public Double getValorpasaje() {
        return valorpasaje;
    }

    public void setValorpasaje(Double valorpasaje) {
        this.valorpasaje = valorpasaje;
    }
    
    
    
    
    
    
}
