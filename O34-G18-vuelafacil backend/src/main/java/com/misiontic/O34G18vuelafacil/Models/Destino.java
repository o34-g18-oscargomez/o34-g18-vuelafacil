/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.Models;


import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import org.hibernate.annotations.GenericGenerator;


/**
 *
 * @author FRANK
 */

@Table
@Entity(name="Destino")
public class Destino implements Serializable{
    
 @Id
 @GeneratedValue(strategy=GenerationType.IDENTITY)
 @Column(name=("idDestino"))
 private Integer iddestino;
    
 @Column(name=("aereopuertoDestino"))
 private String aereopuertodestino;
    
 @Column(name=("ciudadDestino"))
 private String ciudaddestino;  

    public Integer getIddestino() {
        return iddestino;
    }

    public void setIddestino(Integer iddestino) {
        this.iddestino = iddestino;
    }

    public String getAereopuertodestino() {
        return aereopuertodestino;
    }

    public void setAereopuertodestino(String aereopuertodestino) {
        this.aereopuertodestino = aereopuertodestino;
    }

    public String getCiudaddestino() {
        return ciudaddestino;
    }

    public void setCiudaddestino(String ciudaddestino) {
        this.ciudaddestino = ciudaddestino;
    }


 
}


