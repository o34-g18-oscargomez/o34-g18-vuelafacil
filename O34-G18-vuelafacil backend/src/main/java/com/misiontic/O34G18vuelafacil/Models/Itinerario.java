/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.Models;



import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
/**
 *
 * @author FRANK
 */

@Table
@Entity(name="Itinerario")

public class Itinerario implements Serializable{
  
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name=("idItinerario"))
    private Integer iditinerario;   
    
    @ManyToOne
    @JoinColumn(name="idOrigen")
    private Origen origen;
    
    @ManyToOne
    @JoinColumn(name="idDestino")
    private Destino destino;    
    
    @Column(name=("fechaItinerario"))
    private String fechaitinerario;  

    public Integer getIditinerario() {
        return iditinerario;
    }

    public void setIditinerario(Integer iditinerario) {
        this.iditinerario = iditinerario;
    }

    public Origen getOrigen() {
        return origen;
    }

    public void setOrigen(Origen origen) {
        this.origen = origen;
    }

    public Destino getDestino() {
        return destino;
    }

    public void setDestino(Destino destino) {
        this.destino = destino;
    }

    public String getFechaitinerario() {
        return fechaitinerario;
    }

    public void setFechaitinerario(String fechaitinerario) {
        this.fechaitinerario = fechaitinerario;
    }
    
    
}
