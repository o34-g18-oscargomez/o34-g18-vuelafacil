/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.Models;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import org.hibernate.annotations.GenericGenerator;



@Table
@Entity(name="Compañia")
/**
 *
 * @author FRANK
 */
public class Compañia implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name=("idCompañia"))
    private Integer idCompañia;
    
    @Column(name=("nombreCompañia"))
    private String nombrecompañia;
    
    @Column(name=("dirCompañia"))
    private String dircompañia;
    
    @Column(name=("telefonoCompañia"))
    private String telefonocompañia;

    public Integer getIdCompañia() {
        return idCompañia;
    }

    public void setIdCompañia(Integer idCompañia) {
        this.idCompañia = idCompañia;
    }

    public String getNombrecompañia() {
        return nombrecompañia;
    }

    public void setNombrecompañia(String nombrecompañia) {
        this.nombrecompañia = nombrecompañia;
    }

    public String getDircompañia() {
        return dircompañia;
    }

    public void setDircompañia(String dircompañia) {
        this.dircompañia = dircompañia;
    }

    public String getTelefonocompañia() {
        return telefonocompañia;
    }

    public void setTelefonocompañia(String telefonocompañia) {
        this.telefonocompañia = telefonocompañia;
    }
    
    
    
}
