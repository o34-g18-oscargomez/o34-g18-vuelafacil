/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.Service;

import com.misiontic.O34G18vuelafacil.Models.Origen;
import java.util.List;
/**
 *
 * @author FRANK
 */
public interface OrigenService {
    public Origen save(Origen origen);
    public void delete (Integer id);
    public Origen findById (Integer id);
    public List <Origen> findAll();
    
}
