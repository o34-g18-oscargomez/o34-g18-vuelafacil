/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.Service;

import com.misiontic.O34G18vuelafacil.Models.Cliente;
import java.util.List;

/**
 *
 * @author FRANK
 */
public interface ClienteService {
    public Cliente save(Cliente cliente);
    public void delete (Integer id);
    public Cliente findById (Integer id);
    public List <Cliente> findAll();
    
    
}
