/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.Service;

import com.misiontic.O34G18vuelafacil.Models.Vuelos;
import java.util.List;
/**
 *
 * @author FRANK
 */
public interface VuelosService {
    
    public Vuelos save(Vuelos vuelos);
    public void delete (Integer id);
    public Vuelos findById (Integer id);
    public List <Vuelos> findAll();
    
}
