/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.Service;

import com.misiontic.O34G18vuelafacil.Models.Compañia;
import java.util.List;
/**
 *
 * @author FRANK
 */
public interface CompañiaService {
    public Compañia save(Compañia compañia);
    public void delete (Integer id);
    public Compañia findById (Integer id);
    public List <Compañia> findAll();
    
}
