/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.Service;

import com.misiontic.O34G18vuelafacil.Models.Pasajes;
import java.util.List;
/**
 *
 * @author FRANK
 */
public interface PasajesService {
    
    public Pasajes save(Pasajes pasajes);
    public void delete (Integer id);
    public Pasajes findById (Integer id);
    public List <Pasajes> findAll();
    
}
