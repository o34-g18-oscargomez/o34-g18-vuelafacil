/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.Service;

import com.misiontic.O34G18vuelafacil.Models.Destino;
import java.util.List;
/**
 *
 * @author FRANK
 */
public interface DestinoService {
    public Destino save(Destino destino);
    public void delete (Integer id);
    public Destino findById (Integer id);
    public List <Destino> findAll();
     
}
