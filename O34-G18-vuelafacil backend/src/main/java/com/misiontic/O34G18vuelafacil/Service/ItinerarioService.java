/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.O34G18vuelafacil.Service;


import com.misiontic.O34G18vuelafacil.Models.Itinerario;
import java.util.List;
/**
 *
 * @author FRANK
 */
public interface ItinerarioService {
    public Itinerario save(Itinerario itinerario);
    public void delete (Integer id);
    public Itinerario findById (Integer id);
    public List <Itinerario> findAll();
     
}
